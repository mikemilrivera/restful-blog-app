const express = require("express"),
			bodyParser = require("body-parser"),
			mongoose = require("mongoose"),
			methodOverride = require("method-override"),
			expressSanitizer = require("express-sanitizer"),
			app = express(),
			port = process.env.PORT || 5000;

// APP CONFIG
// connect to db
mongoose.connect('mongodb://localhost:27017/restful_blog_app', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify: false
})
.then(() => console.log("Connected to DB"))
.catch(error => console.log(error.message));

app.set("view engine", "ejs");
app.use(express.static("public")); // serve custom stylesheet
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());
app.use(methodOverride("_method"));

// MONGOOSE/MODEL CONFIG
const blogSchema = new mongoose.Schema({
	title: String,
	image: String,
	body: String,
	created: {
		type: Date,
		default: Date.now()
	}
});

const Blog = mongoose.model("Blog", blogSchema);

// RESTFUL ROUTES
app.get("/", (req,res) => {
	res.redirect("/blogs"); //redirect to /blogs GET route
})

// INDEX ROUTE
app.get("/blogs", (req,res) => {
	Blog.find({}, (err, blogs) => {
		if(err){
			console.log("ERROR!");
		} else {
			res.render("index", {blogs: blogs});
		}
	})
})

// NEW ROUTE
app.get("/blogs/new", (req,res) => {
	res.render("new");
})

// CREATE ROUTE
app.post("/blogs", (req,res) => {
	// create blog
	req.body.blog.body = req.sanitize(req.body.blog.body);
	Blog.create(req.body.blog, (err, newBlog) => {
		if(err){
			res.render("new");
		} else {
			//then, redirect to the index
			res.redirect("/blogs");
		}
	})
})

// SHOW ROUTE
app.get("/blogs/:id", (req,res) => {
	Blog.findById(req.params.id, (err, foundBlog) => {
		if(err){
			res.redirect("/blogs");
		} else {
			res.render("show", {blog: foundBlog})
		}
	})
})

// EDIT ROUTE
app.get("/blogs/:id/edit", (req,res) => {
	Blog.findById(req.params.id, (err, foundBlog) => {
		if(err){
			res.redirect("/blogs");
		} else {
			res.render("edit", {blog: foundBlog})
		}
	})
})

// UPDATE ROUTE
app.put("/blogs/:id", (req,res) => {
	req.body.blog.body = req.sanitize(req.body.blog.body);
	Blog.findByIdAndUpdate(req.params.id, req.body.blog, (err, updatedBlog) => {
		if(err){
			res.redirect("/blogs");
		} else {
			res.redirect("/blogs/" + req.params.id);
		}
	})
})

// DESTROY ROUTE
app.delete("/blogs/:id", (req,res) => {
	// destroy blog
	Blog.findByIdAndRemove(req.params.id, (err) => {
		if(err){
			res.redirect("/blogs");
		} else {
			res.redirect("/blogs");
		}
	})
})

// listener
app.listen(port, () => {
	console.log(`RESTful Blog App is listening to port ${port}.`);
});
